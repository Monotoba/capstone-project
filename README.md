# Capstone Project
## Udacity Android Developer Nano Degree 
### (Google Scholarship)

This repo holds the project proposal for my Udacity Android Nano Degree program's Capstone Project. The actual project repo is my Aviation Weather App v2. repository. 

The development of this app was challenging due to the api chosen. The chosen api had mixed responses
that required creation of custome support modules for Retrofit, to handle the api responses that arrived
in html on error. Json on success, and plain text at times (just for good measure). I'm sure the api 
developers simply forgot to include soap and xml responses. I'm sure the design of the api was meant to 
that ensure only those who reach a Zen level of understanding are granted access to their precious data ;->

Who in their right mind designs an endpoint that can produce three different output formats for their 
responses? 